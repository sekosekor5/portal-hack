# M5StickC Plus and M5StickC Plus 2 PORTAL.HACK Firmware

Firmware for advanced security tests on M5StickC Plus and M5StickC Plus 2 ESP32 Devices

## BUY COMPLETE FIRMWARE
🚨**Important:** Scroll down for the link to purchase the complete firmware with all described functions.

![PORTAL.HACK Matrix Logo](https://imagedelivery.net/95QNzrEeP7RU5l5WdbyrKw/9d3acc2b-966d-40d0-170c-30c51cb65a00/shopitem)
FIRMWARE BY @MASTERBUDZ_REAL

FIRMWARE VIDEOS:
https://www.tiktok.com/@master.portalhack

## PORTAL.HACK FIRMWARE FOR M5StickC Plus and M5StickC Plus 2
PORTAL.HACK is an intriguing firmware project developed for M5StickC Plus and M5StickC Plus 2, created to explore the capabilities of ESP32 development with the Arduino IDE. This firmware incorporates various advanced features for educational and experimental purposes.

## Features
* **TV B-Gone:** Ported from MrArm's HAKRWATCH, capable of shutting off many infrared-controlled TVs, projectors, and devices.
* **AppleJuice:** iOS Bluetooth device pairing spam.
* **Bluetooth Device Notification Spamming:** For SwiftPair on Windows and Android.
* **WiFi Spam:** Funny SSIDs, WiFi Rickrolling, and a Random mode that creates hundreds of randomly-named SSIDs per minute.
* **WiFi PORTAL.HACK:** A captive portal for social engineering email credentials, saving usernames, and passwords to an SD Card (if inserted into a supported reader).
* **WiFi SSID Scanner:** Displays 2.4 GHz SSIDs nearby, provides information about them, and allows cloning of SSIDs in PORTAL.HACK.
* **User-adjustable 24 Hour Digital Clock:** Backed by the M5 Stick RTC for stable timekeeping even in deep sleep and low battery mode.
* **EEPROM-backed Settings:** Customizable rotation, brightness, automatic dimming, PORTAL.HACK SSID, battery level, and credits.

## Advanced Features
* **Captive Portal with OTP and Data Grabbing:** Captures user data, including email and OTP, during portal login attempts.

* **RF MHz Jammer:**
  Convert your M5StickC Plus into an RF signal disruptor. Toggle a square wave signal with a button press, visually indicating "RF Jammer JAMMING STARTED" or "RF Jammer JAMMING STOPPED." On-screen instructions guide you to connect the transmitter module to pin 26.

* **Tone Jammer:**
  Emits audio signals in the RF frequency range for added interference and disruption.

* **Continuous RF RollBack Attack:**
  Generates continuous RF signals designed for a RollBack attack, creating interference that disrupts communication without being detected by RF signal copying devices commonly used with remote controls. This feature is particularly effective in scenarios where unauthorized copying of RF signals is a security concern, providing an additional layer of defense against illicit access or control.

* **Sound Noise Buzzer:** Emits a continuous annoying beep for added irritation.
* **Red LED Control:** Allows turning on and off a bright red LED for visual effects.
* **Bluetooth Scanning:** Scans for nearby Bluetooth devices.
* **Tamagotchi Simulator:** A playful virtual pet simulation on the M5StickC Plus display.

* **RF Copy and Replay:** 
    Copies and replays RF signals (requires external RF receiver/transmitter). FS1000A/XY-MK-5V, WITH PINS CONNECTED TO: GND-GND, VCC-5v, RECEIVER_PIN = 25 - TRANSMITTER_PIN = 26;

* **RFID Copy and Replay:** Copies and replays NFC signals when connected to the correct UART device. SDA_PIN 25 - SCK_PIN 26
* **Traffic Lights Jam:** Manipulates infrared signals to display green traffic lights at 14 Hz.

* **RF Bruteforce:**
    Utilizes precise binary codes for access control transmitted to the antenna module, streamlining security system attacks and significantly reducing the time needed to test all possible codes.


* **Wave Bruteforce:** 
    is a cutting-edge project that utilizes RF waveform curves to generate random and unique radio sequences. This innovative experiment 
    focuses on cybersecurity, exploring new possibilities through interaction with radio communication devices.

* **RFID Mifare Attack:** 
    Leveraging the MFRC522 reader to clone RFID cards, emulate radio signals, and compromise Mifare system security. The objective is to gain unauthorized access through the manipulation of radio communications.

* **IR BALACLAVA:** 
    Transform your M5StickC Plus into an "IR Balaclava" by emitting an infrared LED. When positioned near the face with the LED pointing towards cameras, it creates a virtual balaclava that disrupts facial recognition. Pressing a button activates or deactivates the LED with visual instructions.
    
* **DOOR BRUTEFORCE:** 
    Efficiently tests security system codes using the De Bruijn sequence algorithm, systematically exploring bit combinations for faster and more effective code detection.

* **TESLA SEND:**
    Send RF can open all around Tesla Charging Port

* **FAN RF BRUTE:**
    Disrupting public ceiling fans with random RF frequencies for universal control, using innovative waveform curves for enhanced security insights.

* **NFC SCANNER:**
    Advanced scanning with NFC SCANNER unveils UID details for around 95% of NFC cards, offering insights into a variety of cards, from entry passes to credit cards.


## User Interface
Three main controls:
* **Home:** Stops the current process and returns to the menu.
* **Next:** Moves the cursor to the next menu option. In function modes, it stops the process and returns to the previous menu.
* **Select:** Activates the currently-selected menu option and wakes up the dimmed screen in function modes.

**Controls:**
* **Power:** Long-press the power button for 6 seconds to turn off the unit.
* **Home:** Tap the power button.
* **Next:** Tap the side button.
* **Select:** Tap the M5 button on the front of the unit.

## PORTAL.HACK Portal
In PORTAL.HACK Portal mode, an open WiFi Hotspot named "Vodafone Free WiFi" (configurable by clone another AP) is activated. It serves a fake login page for social engineering attacks, capturing entered usernames, passwords, emails, and OTPs. Cloning existing SSIDs is possible from the WiFi Scan details. 
Captured credentials can be viewed by connecting to the portal and browsing to http://172.0.0.1/creds. Custom SSIDs and settings can be configured in a similar manner. 
SD Card support logs usernames and passwords to portal-hack-creds.txt.

*Note: PORTAL.HACK is intended for professional engagements, education, or demonstration purposes only. Unauthorized use of personal information is against the law.*

## Installation for M5StickC Plus and M5StickC Plus 2
The firmware can be installed using WEB ESP-FLASHER for a hassle-free experience:
1. Launch https://esp.huhn.me/
2. Select Correct "COM" port from the menu.
3. Open in the last line the file "PORTAL-HACK-DEMO.bin".
4. CLICK FLASH
5. Enjoy the firmware!

## BUY COMPLETE FIRMWARE

**Buy complete firmware with all described functions here:**

https://masterbudz01.mysellix.io/it/product/portal-hack-device-firmware



**Download demo here:**

https://gitlab.com/teomastro01/portal-hack/-/blob/main/PORTAL.HACK-DEMO.bin

🚨Important Note: The demo firmware is compatible only with M5StickC Plus.


**Where you can buy the device:**

https://aliexpress.com/item/1005006371086676.html


**Additional Modules:**

![FS1000A Transmitter and Receiver Kit](https://5.imimg.com/data5/SELLER/Default/2023/12/369137165/HQ/NS/HL/14495069/fs1000a-433m-transmitter-and-receiver-kit.jpg)


![Home Assistant Community](https://community-assets.home-assistant.io/original/3X/c/9/c9bca4c8b6bbf084cb3b7bd568198f4b3d4a9d75.jpg)


![M5 Stack Module](https://www.mouser.it/images/marketingid/2021/img/121460232.png?v=070223.0329)


![nfc](https://isehara-3lv.sakura.ne.jp/blog/wp-content/uploads/2023/02/IMG_2103-scaled.jpeg)
